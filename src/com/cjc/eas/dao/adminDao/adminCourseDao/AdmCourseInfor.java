package com.cjc.eas.dao.adminDao.adminCourseDao;

import com.cjc.eas.domain.Course;
import com.cjc.eas.utils.ConnectDatabaseUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName AdmCourseInfor
 * @Description TODO 管理员对课程的操作
 * @Author fades
 * @Date 2021/5/31 22:08
 * @Version 1.0
 **/

public class AdmCourseInfor {
    static Connection conn = null;
    static{
        conn = ConnectDatabaseUtils.getConection();
    }

    /**
      *@Description  通过couCno,couName,couType,couDepartment,couCredit,couTeacher,couStatus关键字的任意组合，查询课程信息
      *@params
      *@return
      *@author fades
      *@since
      */
    public static List<Course> QueryCourseInfor(Course course){
        List<Course> result = new ArrayList<>();
        int i = 0;
        StringBuffer sql = new StringBuffer("select couCno,couName,couType,couDepartment,couCredit,couTeacher,couStatus from t_Course where 1=1");
        if(course.getCouName() != null && course.getCouName() != ""){
            sql.append("and couName='");
            sql.append(course.getCouName());
            sql.append("'");
        }
        if(course.getCouCno() != null && course.getCouCno() != ""){
            sql.append("and couCno='");
            sql.append(course.getCouCno());
            sql.append("'");
        }
        if(course.getCouTeacher() != null && course.getCouTeacher() != ""){
            sql.append("and couTeacher='");
            sql.append(course.getCouTeacher());
            sql.append("'");
        }
        if(course.getCouType() != null && course.getCouType() != ""){
            sql.append("and couType='");
            sql.append(course.getCouType());
            sql.append("'");
        }
        if(course.getCouStatus() != null && course.getCouStatus() != ""){
            sql.append("and couStatus='");
            sql.append(course.getCouStatus());
            sql.append("'");
        }
        if(course.getCouDepartment() != null && course.getCouDepartment() != ""){
            sql.append("and couDepartment='");
            sql.append(course.getCouDepartment());
            sql.append("'");
        }

        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql.toString());
            while(rs.next()){
                String couCno = rs.getString("couCno");
                String couName = rs.getString("couName");
                String couType = rs.getString("couType");
                String couDepartment = rs.getString("couDepartment");
                String couCredit = rs.getString("couCredit");
                String couTeacher = rs.getString("couTeacher");
                String couStatus = rs.getString("couStatus");
                Course course1 = new Course();
                course1.setCouCno(couCno);
                course1.setCouName(couName);
                course1.setCouType(couType);
                course1.setCouDepartment(couDepartment);
                course1.setCouCredit(couCredit);
                course1.setCouTeacher(couTeacher);
                course1.setCouStatus(couStatus);
                result.add(course1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return  result;
    }
}
