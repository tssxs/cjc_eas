package com.cjc.eas.dao.adminDao.adminTchDao;


import com.cjc.eas.dao.TeacherDao;
import com.cjc.eas.domain.Teacher;
import com.cjc.eas.utils.ConnectDatabaseUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName AdmTchInfor
 * @Description TODO 管理员对教师信息的管理
 * @Author fades
 * @Date 2021/5/29 21:21
 * @Version 1.0
 **/

public class AdmTchInfor {
    /*
    *静态代码块
    * 获取数据库连接
     */
    static Connection conn = null;
    static {
        conn = ConnectDatabaseUtils.getConection();
    }

    /**
      *@Description 管理员查询教师信息--------输入任意个关键字，显示出所有匹配的行（条件必须与所有关键字都匹配）
      *@params
      *@return
      *@author fades
      *@since
      */
    public static List<Teacher> QueryTchInfor(Teacher teacher){
        List<Teacher> result = new ArrayList<>();
        int i=0;
        StringBuffer sql = new StringBuffer("select * from t_Teacher where 1=1 and");
        if(teacher.getTchWno() != null && teacher.getTchWno() != ""){
            sql.append(" and tchWno like '%");
            sql.append(teacher.getTchWno());
            sql.append("%'");
        }
        if (teacher.getTchName() != null && teacher.getTchName() != "") {
            sql.append(" and tchName like '%");
            sql.append(teacher.getTchName());
            sql.append("%'");
        }
        if(teacher.getTchDepartment() != null && teacher.getTchDepartment() != ""){
            sql.append(" and tchDepartment like '%");
            sql.append(teacher.getTchDepartment());
            sql.append("%'");
        }
        if(teacher.getTchAcademic() != null && teacher.getTchAcademic() != ""){
            sql.append(" and tchDepartment like '%");
            sql.append(teacher.getTchDepartment());
            sql.append("%'");
        }
        if(teacher.getTchProfessionalTitle() != null && teacher.getTchProfessionalTitle() != ""){
            sql.append(" and tchProfessionalTitle like '%");
            sql.append(teacher.getTchProfessionalTitle());
            sql.append("%'");
        }
        if(teacher.getTchPosition() != null && teacher.getTchPosition() != ""){
            sql.append(" and tchPosition like '%");
            sql.append(teacher.getTchPosition());
            sql.append("%'");
        }

        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql.toString());
            while(rs.next()){

//                String tchWno = rs.getString("tchWno");
//                String tchName = rs.getString("tchName");
//                String tchGender= rs.getString("tchGender");
//                String tchNation= rs.getString("tchNation");
//                String tchPosition = rs.getString("tchPosition");
//                String tchProfessionalTitle = rs.getString("tchProfessionalTitle");
//                String tchAcademic = rs.getString("tchAcademic");
//                String tchDepartment = rs.getString("tchDepartment");
//                String tchContactNum= rs.getString("tchContactNum");
//                String tchWorkTime = rs.getString("tchWorkTime");
//                String tchIntroduction = rs.getString("tchIntroduction");
                /*
                账号、密码 暂时搁置
                 */
//                String tchAccount = rs.getString("tchAccount");
//                String tchPasswd= rs.getString("tchPasswd");
                Teacher teacher1 = TeacherDao.transferStu(rs);

//                teacher1.setTchWno(tchWno);
//                teacher1.setTchName(tchName);
//                teacher1.setTchGender(tchGender);
//                teacher1.setTchNation(tchNation);
//                teacher1.setTchPosition(tchPosition);
//                teacher1.setTchProfessionalTitle(tchProfessionalTitle);
//                teacher1.setTchAcademic(tchAcademic);
//                teacher1.setTchDepartment(tchDepartment);
//                teacher1.setTchContactNum(tchContactNum);
//                teacher1.setTchWorkTime(tchWorkTime);
//                teacher1.setTchIntroduction(tchIntroduction);
                result.add(teacher1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }
}
