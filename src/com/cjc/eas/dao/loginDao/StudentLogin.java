package com.cjc.eas.dao.loginDao;


import com.cjc.eas.utils.GetAccountInfor;

import org.junit.Test;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName StudentLogin
 * @Description TODO 学生登录
 * @Author fades
 * @Date 2021/5/31 20:07
 * @Version 1.0
 **/

public class StudentLogin {
    public static boolean studentLogin(String account ,String passwd){
        Map<String,String> accountMap = GetAccountInfor.getAccoutInfor("student");
        if(accountMap.size()==0){
            System.out.println("获取账号列表失败！请查找原因");
            return false;
        }
        Set<String> accountSet = accountMap.keySet();
        Iterator it = accountSet.iterator();
        while(it.hasNext()){
            Object itemAccount = it.next();
            if (itemAccount.equals(account)) {
                Object itemPasswd = accountMap.get(itemAccount);
                if(itemPasswd.equals(passwd)){
                    System.out.println("登录成功！");
                    return true;
                }else{
                    System.out.println("密码错误！");
                    return false;
                }
            }
            System.out.println("用户名不存在！");
        }
        return  false;
    }

    @Test
    public void tes(){

    }

}
