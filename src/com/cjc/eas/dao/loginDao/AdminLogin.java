package com.cjc.eas.dao.loginDao;

import com.cjc.eas.ui.studentFrame.StuFrame1;
import com.cjc.eas.utils.GetAccountInfor;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName AdminLogin
 * @Description TODO 管理员登录
 * @Author fades
 * @Date 2021/5/31 21:33
 * @Version 1.0
 **/

public class AdminLogin {
    public static boolean adminLogin(String account ,String passwd){
        Map<String,String> accountMap = GetAccountInfor.getAccoutInfor("admin");
        if(accountMap.size()==0){
            System.out.println("获取账号列表失败！请查找原因");
            return false;
        }
        Set<String> accountSet = accountMap.keySet();
        Iterator it = accountSet.iterator();
        while(it.hasNext()){
            Object itemAccount = it.next();
            if (itemAccount.equals(account)) {
                Object itemPasswd = accountMap.get(itemAccount);
                if(itemPasswd.equals(passwd)){
                    System.out.println("登录成功!");
                    return true;
                }else{
                    System.out.println("密码错误！");
                    return false;
                }
            }
            System.out.println("用户名不存在！");
        }
        return  false;
    }

}
