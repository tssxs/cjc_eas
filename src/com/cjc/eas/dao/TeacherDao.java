package com.cjc.eas.dao;

import com.cjc.eas.domain.Student;
import com.cjc.eas.domain.Teacher;
import com.cjc.eas.utils.DataBaseConnectUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * ClassName :TeacherDao <br/>
 * Date :2021/5/28 21:40 <br/>
 *
 * @author :GL <br/>
 * @since :jdk 1.8
 */
public class TeacherDao {
    static private Connection conection = null;
    static {
        conection = DataBaseConnectUtils.getConection();
    }
    public Teacher getStudentById(String Wno){
        Teacher teacher = new Teacher();
        try{
            PreparedStatement statement = conection.prepareStatement("select * from t_student where id = ?");
            statement.setString(1,Wno);
            try{
                ResultSet resultSet = statement.executeQuery();
                 teacher = transferStu(resultSet);

                return teacher;
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }finally {
            try {
                if (conection != null){
                    conection.close();
                }
                conection = null;
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
        return teacher;
    }

    private Teacher transferStu(ResultSet resultSet) throws Exception{
        Teacher teacher = new Teacher();
        teacher.setTchWno(resultSet.getString(1));
        teacher.setTchName(resultSet.getString(2));
        teacher.setTchGender(resultSet.getString(3));
        teacher.setTchPosition(resultSet.getString(4));
        teacher.setTchNation(resultSet.getString(5));
        teacher.setTchProfessionalTitle(resultSet.getString(6));
        teacher.setTchAcademic(resultSet.getString(7));
        teacher.setTchDepartment(resultSet.getString(8));
        teacher.setTchContactNum(resultSet.getString(9));
        teacher.setTchWorkTime(resultSet.getString(10));
        teacher.setTchIntroduction(resultSet.getString(11));

        return teacher;
    }
}
