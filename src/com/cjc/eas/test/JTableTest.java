package com.cjc.eas.test;

import com.cjc.eas.ui.GridBagTable;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;

/**
 * @param param
 * @ClassName JTableTest
 * @Description //TODO
 * @Date 2021/5/262:05
 * @Author tssxs_001@163.com
 * @Version 1.0
 **/

public class JTableTest {
    @Test
   public void JTableBaseTest(){

        JPanel jPanel = new JPanel();
        jPanel.setSize(300,400);
        jPanel.setVisible(true);
        JTable jTable = new JTable();
        jTable.getRowMargin();
   }
   public static void main(String [] args){


        JPanel jPanel = new JPanel();
       jPanel.setSize(300,400);
       jPanel.setVisible(true);
       jPanel.setEnabled(true);

       //表头数据
       String[] columnNames = {"First Name", "Last Name",
               "Sport","# of Years", "Vegetarian"};
       //表内容
       Object [][] data = {
               { "Kathy", "Smith", "Snowboarding", new Integer(5), new Boolean(false) },
               { "John", "Doe", "Rowing", new Integer(3), new Boolean(true) },
               { "Sue", "Black", "Knitting", new Integer(2), new Boolean(false) },
               { "Jane", "White", "Speed reading", new Integer(20), new Boolean(true) },
               { "Joe", "Brown", "Pool", new Integer(10), new Boolean(false) }
       };
       //JTable jTable = new JTable(data,columnNames);
       GridBagTable jTable = new GridBagTable(data,columnNames);
       jTable.setBackground(Color.cyan);
       jTable.setRowHeight(50);
       //int [] rows = {2,3};
       //int [] cols = {2,3};
       jTable.mergeCells(new int[]{0, 1},new int[] {0,1});

       JScrollPane scrollPane = new JScrollPane(jTable);
       JFrame jFrame = new JFrame();
       jFrame.setTitle("表格控件测试");
       jFrame.setSize(600,480);
       jFrame.setVisible(true);
       jFrame.setContentPane(scrollPane);
   }
}
