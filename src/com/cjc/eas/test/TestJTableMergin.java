package com.cjc.eas.test;

import com.cjc.eas.ui.GridBagTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @param
 * @ClassName TestJTableMergin
 * @Description //TODO
 * @Date 2021/5/3116:47
 * @Author tssxs_001@163.com
 * @Version 1.0
 **/

public class TestJTableMergin implements ActionListener {
    GridBagTable table;
    public TestJTableMergin()
    {
        JFrame d = new JFrame();
        DefaultTableModel model = new DefaultTableModel(5,5);

        table = new GridBagTable(model);
        table.setRowHeight(40);

        JScrollPane pane = new JScrollPane(table);
        d.getContentPane().add(pane, BorderLayout.CENTER);
        JButton btn = new JButton("合并/拆分");
        d.getContentPane().add(btn, BorderLayout.NORTH);
        btn.addActionListener(this);
        d.setBounds(0, 0, 400, 400);
        d.setVisible(true);
    }

    public static void main(String[] fsd){
        new TestJTableMergin();
    }

    public void actionPerformed(ActionEvent e) {
        table.mergeCells(table.getSelectedRows(), table.getSelectedColumns());
    }
}
