package com.cjc.eas.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName getAccountInfor
 * @Description TODO 获取数据库中的相应的账户密码，为登录服务
 * @Author fades
 * @Date 2021/5/31 20:27
 * @Version 1.0
 **/

public class GetAccountInfor {
    //获取数据库连接
    static Connection conn = ConnectDatabaseUtils.getConection();
    public static Map<String,String> getAccoutInfor(String accountType){
        String sql = "";
        if(accountType.equals("student")){
            sql = "select stuAccount,stuPasswd from t_Student ";
        }else if(accountType.equals("teacher")){
            sql = "select tchAccount,tchPasswd from t_Teacher ";
        }
        else if(accountType.equals("admin")){
            sql = "select adminAccount,adminPasswd from t_Admin ";
        }
        Statement stmt = null;
        ResultSet rs =null;
        Map<String,String> accountList = new HashMap<String,String>(){
        };
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while(rs.next()) {
                String acc = rs.getString(1);
                String pas = rs.getString(2);
                accountList.put(acc,pas);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return  accountList;
    }
}
