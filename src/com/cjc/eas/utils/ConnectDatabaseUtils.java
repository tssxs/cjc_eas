package com.cjc.eas.utils;

/**
 * @ClassName ConnectDatabaseUtils
 * @Description 获取数据库的连接
 * @Author fades
 * @Date 2021/5/28 21:38
 * @Version 1.0
 **/
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

    /**
     * ClassName :DataBaseConnectUtils <br/>
     * Date :2021/5/28 19:21 <br/>
     *
     * @author :GL <br/>
     * @since :jdk 1.8
     */
    public class ConnectDatabaseUtils {

        /**
         * 主要用来获取数据库配置文件
         *
         * @return
         * @throws Exception
         */
        public static Properties getDataBaseProperties() {
            Properties pro = new Properties();
            try {
                FileInputStream in = new FileInputStream("res/database.properties");
                pro.load(in);
                in.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                return pro;
            }
        }

        public static Connection getConection() {
            Connection con = null;
            try {
                Properties pros = getDataBaseProperties();
                String url = (String) pros.get("url");
                String className = (String) pros.get("className");
                String userName = (String) pros.get("userName");
                String password = (String) pros.get("password");

                Class.forName(className);
                con = DriverManager.getConnection(url, userName, password);
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
            return con;
        }
    }
