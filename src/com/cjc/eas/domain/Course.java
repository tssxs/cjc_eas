package com.cjc.eas.domain;

/**
 * @ClassName Course
 * @Description TODO 课程类
 * @Author fades
 * @Date 2021/5/31 18:21
 * @Version 2.0
 **/

public class Course {
    private String couCno;
    private String couName;
    private String couType;
    private String couCredit;
    private String couTeacher;
    private String couWno;
    private String couStatus;
    private String couDepartment;

    public String getCouDepartment() {
        return couDepartment;
    }

    public void setCouDepartment(String couDepartment) {
        this.couDepartment = couDepartment;
    }



    public String getCouCno() {
        return couCno;
    }

    public String getCouName() {
        return couName;
    }

    public String getCouType() {
        return couType;
    }

    public String getCouCredit() {
        return couCredit;
    }

    public String getCouTeacher() {
        return couTeacher;
    }

    public String getCouWno() {
        return couWno;
    }

    public String getCouStatus() {
        return couStatus;
    }

    public void setCouCno(String couCno) {
        this.couCno = couCno;
    }

    public void setCouName(String couName) {
        this.couName = couName;
    }

    public void setCouType(String couType) {
        this.couType = couType;
    }

    public void setCouCredit(String couCredit) {
        this.couCredit = couCredit;
    }

    public void setCouTeacher(String couTeacher) {
        this.couTeacher = couTeacher;
    }

    public void setCouWno(String couWno) {
        this.couWno = couWno;
    }

    public void setCouStatus(String couStatus) {
        this.couStatus = couStatus;
    }
}
