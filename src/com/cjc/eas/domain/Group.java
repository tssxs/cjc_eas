package com.cjc.eas.domain;

/**
 * @ClassName Group
 * @Description TODO 班级类
 * @Author fades
 * @Date 2021/5/31 18:18
 * @Version 1.0
 **/

public class Group {
    private String groClassNum;
    private String groTname;
    private String groWno;
    private String groMajor;
    private String groDepartment;

    public String getGroClassNum() {
        return groClassNum;
    }

    public String getGroTname() {
        return groTname;
    }

    public String getGroWno() {
        return groWno;
    }

    public String getGroMajor() {
        return groMajor;
    }

    public String getGroDepartment() {
        return groDepartment;
    }

    public void setGroClassNum(String groClassNum) {
        this.groClassNum = groClassNum;
    }

    public void setGroTname(String groTname) {
        this.groTname = groTname;
    }

    public void setGroWno(String groWno) {
        this.groWno = groWno;
    }

    public void setGroMajor(String groMajor) {
        this.groMajor = groMajor;
    }

    public void setGroDepartment(String groDepartment) {
        this.groDepartment = groDepartment;
    }
}
