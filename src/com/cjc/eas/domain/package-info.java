/**
 * 说明：此包中专门存放系统也对就的实体类，这些实体类中只包含实体属性及get,set方法
 * @author tssxs_001@163.com
 * @date:2021-05-23
 * @version 1.0
 */
package com.cjc.eas.domain;