package com.cjc.eas.domain;

/**
 * @ClassName Admin
 * @Description TODO 管理员类
 * @Author fades
 * @Date 2021/5/29 10:50
 * @Version 1.0
 **/

public class Admin {
    private String adminAccount="";
    private String adminPasswd="";

    public String getAdminAccount() {
        return adminAccount;
    }

    public String getAdminPasswd() {
        return adminPasswd;
    }

    public void setAdminAccount(String adminAccount) {
        this.adminAccount = adminAccount;
    }

    public void setAdminPasswd(String adminPasswd) {
        this.adminPasswd = adminPasswd;
    }
}
