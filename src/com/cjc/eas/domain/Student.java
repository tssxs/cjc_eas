package com.cjc.eas.domain;

/**
 * @ClassName Admin
 * @Description TODO 学生类
 * @Author
 * @Date
 * @Version 1.0
 **/
public class Student {
    private String stuSno;      //学号
    private String stuName;         //姓名
    private String stuGender;   //性别
    private String stuDateOfBirth;  //出生日期
    private String stuID;       //身份证号
    private String stuClassNum; //班级
    private String stuNation;   //民族
    private String stuDepartment;   //院系
    private String stuContactNum;   //联系电话
    private String stuContactAddr;  //联系地址
    private String stuContactPerson;    //联系人
    private String stuEnrollmentYear;   //入学年份
    private String stuEduLevel;     //培养层次
    private String stuMajor;    //专业
    private String stuLengthOfStudy;    //学制
    private String stuPostcode;     //邮政编码
    private String stuNativePlace;  //籍贯
    private String stuNativePlaceType;  //户籍类型
    private String stuEmail;    //邮箱地址
    private String stuAccount;  //账户
    private String stuPasswd;   //密码
    private String stuPostCard; //

    public String getStuSno() {
        return stuSno;
    }

    public void setStuSno(String stuSno) {
        this.stuSno = stuSno;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getStuGender() {
        return stuGender;
    }

    public void setStuGender(String stuGender) {
        this.stuGender = stuGender;
    }

    public String getStuDateOfBirth() {
        return stuDateOfBirth;
    }

    public void setStuDateOfBirth(String stuDateOfBirth) {
        this.stuDateOfBirth = stuDateOfBirth;
    }

    public String getStuID() {
        return stuID;
    }

    public void setStuID(String stuID) {
        this.stuID = stuID;
    }

    public String getStuDepartment() {
        return stuDepartment;
    }

    public void setStuDepartment(String stuDepartment) {
        this.stuDepartment = stuDepartment;
    }

    public String getStuContactNum() {
        return stuContactNum;
    }

    public void setStuContactNum(String stuContactNum) {
        this.stuContactNum = stuContactNum;
    }

    public String getStuClassNum() {
        return stuClassNum;
    }

    public void setStuClassNum(String stuClassNum) {
        this.stuClassNum = stuClassNum;
    }

    public String getStuNation() {
        return stuNation;
    }

    public void setStuNation(String stuNation) {
        this.stuNation = stuNation;
    }


    public String getStuContactAddr() {
        return stuContactAddr;
    }

    public void setStuContactAddr(String stuContactAddr) {
        this.stuContactAddr = stuContactAddr;
    }

    public String getStuContactPerson() {
        return stuContactPerson;
    }

    public void setStuContactPerson(String stuContactPerson) {
        this.stuContactPerson = stuContactPerson;
    }

    public String getStuEnrollmentYear() {
        return stuEnrollmentYear;
    }

    public void setStuEnrollmentYear(String stuEnrollmentYear) {
        this.stuEnrollmentYear = stuEnrollmentYear;
    }

    public String getStuEduLevel() {
        return stuEduLevel;
    }

    public void setStuEduLevel(String stuEduLevel) {
        this.stuEduLevel = stuEduLevel;
    }

    public String getStuMajor() {
        return stuMajor;
    }

    public void setStuMajor(String stuMajor) {
        this.stuMajor = stuMajor;
    }

    public String getStuLengthOfStudy() {
        return stuLengthOfStudy;
    }

    public void setStuLengthOfStudy(String stuLengthOfStudy) {
        this.stuLengthOfStudy = stuLengthOfStudy;
    }

    public String getStuPostcode() {
        return stuPostcode;
    }

    public void setStuPostcode(String stuPostCard) {
        this.stuPostcode = stuPostCard;
    }

    public String getStuNativePlace() {
        return stuNativePlace;
    }

    public void setStuNativePlace(String stuNativePlace) {
        this.stuNativePlace = stuNativePlace;
    }

    public String getStuNativePlaceType() {
        return stuNativePlaceType;
    }

    public void setStuNativePlaceType(String stuNativePlaceType) {
        this.stuNativePlaceType = stuNativePlaceType;
    }

    public String getStuEmail() {
        return stuEmail;
    }

    public void setStuEmail(String stuEmail) {
        this.stuEmail = stuEmail;
    }

    public String getStuAccount() {
        return stuAccount;
    }

    public void setStuAccount(String stuAccount) {
        this.stuAccount = stuAccount;
    }

    public String getStuPasswd() {
        return stuPasswd;
    }

    public void setStuPasswd(String stuPasswd) {
        this.stuPasswd = stuPasswd;
    }

    public String getStuPostCard() {
        return stuPostCard;
    }

    public void setStuPostCard(String stuPostCard) {
        this.stuPostCard = stuPostCard;
    }
}
