package com.cjc.eas.domain;

/**
 * @ClassName Admin
 * @Description TODO 教师类
 * @Author
 * @Date
 * @Version 1.0
 **/

public class Teacher {
    private String tchWno;  //职工号
    private String tchName; //姓名
    private String tchGender;   //性别
    private String tchPosition; //职位
    private String tchNation;
    private String tchProfessionalTitle;    //职称
    private String tchAcademic; //学历
    private String tchDepartment;   //院系
    private String tchContactNum;   //联系方式
    private String tchWorkTime;     //工作时长
    private String tchIntroduction;     //简介
    private String tchAccount;  //账号
    private String tchPasswd;   //密码

    public String getTchWno() {
        return tchWno;
    }

    public void setTchWno(String tchWno) {
        this.tchWno = tchWno;
    }

    public String getTchName() {
        return tchName;
    }

    public void setTchName(String tchName) {
        this.tchName = tchName;
    }

    public String getTchGender() {
        return tchGender;
    }

    public void setTchGender(String tchGender) {
        this.tchGender = tchGender;
    }

    public String getTchPosition() {
        return tchPosition;
    }

    public void setTchPosition(String tchPosition) {
        this.tchPosition = tchPosition;
    }
    public String getTchNation() {
        return tchNation;
    }

    public void setTchNation(String tchNation) {
        this.tchNation = tchNation;
    }


    public String getTchProfessionalTitle() {
        return tchProfessionalTitle;
    }

    public void setTchProfessionalTitle(String tchProfessionalTitle) {
        this.tchProfessionalTitle = tchProfessionalTitle;
    }

    public String getTchAcademic() {
        return tchAcademic;
    }

    public void setTchAcademic(String tchAcademic) {
        this.tchAcademic = tchAcademic;
    }

    public String getTchDepartment() {
        return tchDepartment;
    }

    public void setTchDepartment(String tchDepartment) {
        this.tchDepartment = tchDepartment;
    }

    public String getTchContactNum() {
        return tchContactNum;
    }

    public void setTchContactNum(String tchContactNum) {
        this.tchContactNum = tchContactNum;
    }

    public String getTchWorkTime() {
        return tchWorkTime;
    }

    public void setTchWorkTime(String tchWorkTime) {
        this.tchWorkTime = tchWorkTime;
    }

    public String getTchIntroduction() {
        return tchIntroduction;
    }

    public void setTchIntroduction(String tchIntroduction) {
        this.tchIntroduction = tchIntroduction;
    }

    public String getTchAccount() {
        return tchAccount;
    }

    public void setTchAccount(String tchAccount) {
        this.tchAccount = tchAccount;
    }

    public String getTchPasswd() {
        return tchPasswd;
    }

    public void setTchPasswd(String tchPasswd) {
        this.tchPasswd = tchPasswd;
    }


}
