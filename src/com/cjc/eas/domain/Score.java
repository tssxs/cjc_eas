package com.cjc.eas.domain;

/**
 * @ClassName SelectCourse
 * @Description TODO 学生成绩类
 * @Author fades
 * @Date 2021/5/31 18:23
 * @Version 1.0
 **/

public class Score {
    private String scoSno;      //学号
    private String scoCno;      //课程号
    private String scoName;     //课程名
    private String scoWno;      //职工号
    private String scoTeacher;  //授课教师
    private String scoType;     //类别
    private String scoScore;    //成绩
    private String scoAnnual;   //学年
    private String scoPeriod;   //学期
    private String scoGetCredits;   //所得学分
    private String scoGPA;      //绩点

    public String getScoSno() {
        return scoSno;
    }

    public String getScoCno() {
        return scoCno;
    }

    public String getScoName() {
        return scoName;
    }

    public String getScoWno() {
        return scoWno;
    }

    public String getScoTeacher() {
        return scoTeacher;
    }

    public String getScoType() {
        return scoType;
    }

    public String getScoScore() {
        return scoScore;
    }

    public String getScoAnnual(){
        return scoAnnual;
    }

    public String getScoPeriod() {
        return scoPeriod;
    }

    public String getScoGetCredits() {
        return scoGetCredits;
    }

    public String getScoGPA() {
        return scoGPA;
    }

    public void setScoSno(String scoSno) {
        this.scoSno = scoSno;
    }

    public void setScoCno(String scoCno) {
        this.scoCno = scoCno;
    }

    public void setScoName(String scoName) {
        this.scoName = scoName;
    }

    public void setScoWno(String scoWno) {
        this.scoWno = scoWno;
    }

    public void setScoTeacher(String scoTeacher) {
        this.scoTeacher = scoTeacher;
    }

    public void setScoType(String scoType) {
        this.scoType = scoType;
    }

    public void setScoScore(String scoScore) {
        this.scoScore = scoScore;
    }

    public void setScoAnnual(String scoAnnual) {
        this.scoAnnual = scoAnnual;
    }

    public void setScoPeriod(String scoPeriod) {
        this.scoPeriod = scoPeriod;
    }

    public void setScoGetCredits(String scoGetCredits) {
        this.scoGetCredits = scoGetCredits;
    }

    public void setScoGPA(String scoGPA) {
        this.scoGPA = scoGPA;
    }
}


