package com.cjc.eas.ui.studentFrame;

import com.cjc.eas.service.studentService.StudentService;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;

/**
 * @ClassName StuFrame_Btn2_2
 * @Description TODO 按钮btn2_2的页面
 * @Author fades
 * @Date 2021/6/2 23:58
 * @Version 1.0
 **/

public class StuFrame_Btn2_2 extends JFrame{
    public static void main(String[] args) {
        EventQueue.invokeLater(()->{
            new StuFrame_Btn2_2();
        });

    }

    static String scoAnnual="";
    static String scoPeriod="";

    JTable table = null;
    JScrollPane pane = null;
    Icon image1 = new ImageIcon("E:\\eas图片\\学生栏目.jpg");
    Icon image2 = new ImageIcon("E:\\eas图片\\当前页面背景色.jpg");
    JLabel label_image1=new JLabel(image1);
    JLabel label_image2=new JLabel(image2);
    JButton btn1 = new JButton("公用信息");
    JButton btn2 = new JButton("信息查询");
    JButton btn3 = new JButton("网上选课");
    JButton btn4 = new JButton("活动报名");
    JButton btn5 = new JButton("网上教评");
    JButton btn6 = new JButton("账号管理");
    JButton btn7 = new JButton("安全退出");

    JButton btn2_1 = new JButton(" □ 学籍信息");
    JButton btn2_2 = new JButton(" □ 历年成绩");
    JButton btn2_3 = new JButton(" □ 考试安排");
    JButton btn2_4 = new JButton(" □ 培养计划");

    JButton btnHomePage = new JButton("首页");
    JButton btnReturn = new JButton("<返回");

    JButton btn_query = new JButton("查询");

    Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
    int screenWidth = (int) screensize.getWidth();//获得屏幕得宽
    int screenHeight = (int) screensize.getHeight();//获得屏幕得高

    JComboBox combobox_Annual = new JComboBox();
    JComboBox comboBox_Period = new JComboBox();

    public StuFrame_Btn2_2(){
        MyComponent_Btn2_2 my = new MyComponent_Btn2_2();
        my.setBounds(0,0,screenWidth,screenHeight);
        this.add(my);
        init();
        addComponentContent();

    }
    public void init(){
        this.setLayout(null);
        this.setTitle("B1903_教务系统");
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setBackground(Color.white);
        this.setVisible(true);
        addComponent_left();
    }
    public void addComponent_left(){

        label_image1.setBounds(0,0,1920,219);
        label_image2.setBounds(0,219,1920,55);

        this.add(label_image1);
        this.add(label_image2);

        /*
        设置按钮透明
         */
//        btn1.setContentAreaFilled(false);
        /*
        取消边框
         */
//        btn1.setBorderPainted(false);

        btn1.setBounds(0,219+55+7,308,45);
        btn2.setBounds(0,219+10+(45+7)*2,308,45);
        btn2_1.setBounds(0,219+20+(45+7)*2+35+4,208,35);
        btn2_2.setBounds(0,219+20+(45+7)*2+(35+4)*2,208,35);
        btn2_3.setBounds(0,219+20+(45+7)*2+(35+4)*3,208,35);
        btn2_4.setBounds(0,219+20+(45+7)*2+(35+4)*4,208,35);
        btn3.setBounds(0,219+10+(45+7)*2+(35+4)*4+(45+7),308,45);
        btn4.setBounds(0,219+10+(45+7)*2+(35+4)*4+(45+7)*2,308,45);
        btn5.setBounds(0,219+10+(45+7)*2+(35+4)*4+(45+7)*3,308,45);
        btn6.setBounds(0,219+10+(45+7)*2+(35+4)*4+(45+7)*4,308,45);
        btn7.setBounds(0,screenHeight-130,308,45);


        btn1.setFont(new Font("华文中宋",1,23));
        btn2_1.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn2_2.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn2_3.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn2_4.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn2.setFont(new Font("华文中宋",1,23));
        btn3.setFont(new Font("华文中宋",1,23));
        btn4.setFont(new Font("华文中宋",1,23));
        btn5.setFont(new Font("华文中宋",1,23));
        btn6.setFont(new Font("华文中宋",1,23));
        btn7.setFont(new Font("华文中宋",1,23));

        btn1.setBackground(new Color(149,181,252));
        btn2.setBackground(new Color(149,181,252));
        btn3.setBackground(new Color(149,181,252));
        btn4.setBackground(new Color(149,181,252));
        btn5.setBackground(new Color(149,181,252));
        btn6.setBackground(new Color(149,181,252));
        btn7.setBackground(new Color(149,181,252));

        btn2_1.setBorderPainted(false);
        btn2_1.setContentAreaFilled(false);
        btn2_2.setBorderPainted(false);
        btn2_2.setContentAreaFilled(false);
        btn2_3.setBorderPainted(false);
        btn2_3.setContentAreaFilled(false);
        btn2_4.setBorderPainted(false);
        btn2_4.setContentAreaFilled(false);

        this.add(btn1);
        this.add(btn2);
        this.add(btn3);
        this.add(btn4);
        this.add(btn5);
        this.add(btn6);
        this.add(btn7);

        this.add(btn2_1);
        this.add(btn2_2);
        this.add(btn2_3);
        this.add(btn2_4);

        btnHomePage.setBorderPainted(false);
        btnReturn.setBorderPainted(false);
        btnHomePage.setBackground(new Color(149,181,252));
        btnReturn.setBackground(new Color(149,181,252));
        btnHomePage.setFont(new Font("华文中宋",Font.PLAIN,24));
        btnReturn.setFont(new Font("华文中宋",Font.PLAIN,24));
        btnHomePage.setBounds(1720,227,90,40);
        btnReturn.setBounds(1810,227,100,40);
        this.add(btnHomePage);
        this.add(btnReturn);

        btn_query.setBounds(1620,285,100,40);
        btn_query.setFont(new Font("黑体",Font.PLAIN,28));
        btn_query.setBackground(Color.WHITE);

        this.add(btn_query);

        btn_query.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                scoAnnual=(String) combobox_Annual.getSelectedItem();
                scoPeriod=(String) comboBox_Period.getSelectedItem();
                System.out.println(scoAnnual);
                System.out.println(scoPeriod);
                showTable(StudentService.queryScore_Service(scoAnnual,scoPeriod,"1945867001"));

            }
        });

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn2_2.this.setVisible(false);
                new StuFrame_Btn1();
            }
        });
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn2_2.this.setVisible(false);
                new StuFrame1();
            }
        });
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn2_2.this.setVisible(false);
                new StuFrame_Btn3();
            }
        });
        btn4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn2_2.this.setVisible(false);
                new StuFrame_Btn4();
            }
        });
        btn5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn2_2.this.setVisible(false);
                new StuFrame_Btn5();
            }
        });
        btn6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn2_2.this.setVisible(false);
                new StuFrame_Btn6();
            }
        });
        btn7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }
    public void addComponentContent(){
        combobox_Annual.setFont(new Font("黑体",Font.PLAIN,20));
        combobox_Annual.addItem("  ");
        combobox_Annual.addItem("2020-2021");
        combobox_Annual.addItem("2019-2020");
        combobox_Annual.addItem("2018-2019");
        comboBox_Period.setFont(new Font("黑体",Font.PLAIN,20));
        comboBox_Period.addItem("   ");
        comboBox_Period.addItem("第一学期");
        comboBox_Period.addItem("第二学期");

        combobox_Annual.setBounds(596,285,135,40);
        comboBox_Period.setBounds(881,285,135,40);
        this.add(combobox_Annual);
        this.add(comboBox_Period);

    }

    public void showTable(Object[][] o){
        TableModel_scoreTable model = new TableModel_scoreTable(o);
        table = new JTable(model);
        //设置表头高度40
        table.getTableHeader().setPreferredSize(new Dimension(1,40));
        //设置行高40
        table.setRowHeight(TableModel_scoreTable.rowHeight);
        table.getColumnModel().getColumn(0).setMinWidth(73);
        table.getColumnModel().getColumn(1).setMinWidth(122);
        table.getColumnModel().getColumn(2).setMinWidth(231);
        table.getColumnModel().getColumn(3).setMinWidth(146);
        table.getColumnModel().getColumn(4).setMinWidth(86);
        table.getColumnModel().getColumn(5).setMinWidth(163);
        table.getColumnModel().getColumn(6).setMinWidth(147);
        table.getColumnModel().getColumn(7).setMinWidth(106);
        table.getColumnModel().getColumn(8).setMinWidth(101);
        table.getColumnModel().getColumn(9).setMinWidth(103);

        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();// 设置table内容居中
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        table.setDefaultRenderer(Object.class, tcr);

        //设置表格的背景颜色和字体
        table.setBackground(Color.WHITE);
        table.getTableHeader().setBackground(Color.WHITE);
        table.getTableHeader().setFont(new Font("华文仿宋",1,24));
        table.setFont(new Font("华文仿宋",1,24));

        pane = new JScrollPane(table);
        pane.setBounds(465,410,1278,400);
        this.add(pane);
    }
}

class TableModel_scoreTable extends AbstractTableModel {
    static int rowHeight = 40;

    //调用函数将结果集以二维数组的方式返回，并且通过这个数组的大小确定JscrollPane的高度
//    private Object[][] cells ={
//            {"","","","","","","","","",""},
//            {"","","","","","","","","",""},
//            {"","","","","","","","","",""},
//            {"","","","","","","","","",""},
//            {"","","","","","","","","",""},
//            {"","","","","","","","","",""},
//            {"","","","","","","","","",""},
//            {"","","","","","","","","",""}
//    };
    private  Object[][] cells;
    private static String[] columnNames = {"序号","课程号","课程名","获得学分","类别","授课教师","开设院系","成绩","绩点","备注"};

    public TableModel_scoreTable(Object[][] o) {
        cells=o;
    }
    static int weight=columnNames.length;
    @Override
    public int getRowCount() {
//        System.out.println(cells.length);
        return cells.length;
    }

    @Override
    public int getColumnCount() {
//        System.out.println(cells[0].length);
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return  cells[rowIndex][columnIndex];
    }

    public void setValueAt(Object obj,int rowIndex,int columnIndex){
        cells[rowIndex][columnIndex]=obj;
    }

    public String getColumnName(int c){
        return columnNames[c];
    }

//    public Class<?> getColumnClass(int columnIndex){
//        return cells[0][columnIndex].getClass();
//    }

    public boolean isCellEditable(int rowIndex,int columnIndex){
        return columnIndex == -1;
    }
}

class MyComponent_Btn2_2 extends JComponent{
    public void paintComponent(Graphics g){
        Graphics2D g1=(Graphics2D)g;
//        Rectangle2D.Double  rect = new Rectangle2D.Double(0,219,1920,55);
//        g1.setPaint(new Color(149,181,252));
//        g1.fill(rect);
        g1.draw( new Line2D.Double(308,219+55,308,1080));
        g1.setFont(new Font("华文新魏",Font.PLAIN,32));
        g1.setColor(Color.WHITE);
        g1.drawString("叶鹏",1610,39);
        g1.setFont(new Font("华文中宋",Font.PLAIN,34));
        g1.setColor(Color.BLACK);
        g1.drawString("当前页面———信息查询",308,255);
        g1.setFont(new Font("黑体",1,36));
        g1.drawString("昌吉学院学生成绩明细",892,380);
        g1.setFont(new Font("黑体",Font.PLAIN,28));
        g1.drawString("学年",522,315);
        g1.drawString("学期",809,315);
    }
}
