package com.cjc.eas.ui.studentFrame;

/**
 * @ClassName StuFrame1
 * @Description TODO
 * @Author fades
 * @Date 2021/6/2 16:58
 * @Version 1.0
 **/

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

public class StuFrame1 extends JFrame {

    public static void main(String[] args) {
        new StuFrame1();
    }

    Icon image1 = new ImageIcon("E:\\eas图片\\学生栏目.jpg");
    Icon image2 = new ImageIcon("E:\\eas图片\\当前页面背景色.jpg");
    JLabel label_image1=new JLabel(image1);
    JLabel label_image2=new JLabel(image2);
    JButton btn1 = new JButton("公用信息");
    JButton btn2 = new JButton("信息查询");
    JButton btn3 = new JButton("网上选课");
    JButton btn4 = new JButton("活动报名");
    JButton btn5 = new JButton("网上教评");
    JButton btn6 = new JButton("账号管理");
    JButton btn7 = new JButton("安全退出");


    JButton btnHomePage = new JButton("首页");
    JButton btnReturn = new JButton("<返回");

    Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
    int screenWidth = (int) screensize.getWidth();//获得屏幕得宽
    int screenHeight = (int) screensize.getHeight();//获得屏幕得高

    public StuFrame1(){
        EventQueue.invokeLater(()->{
            MyComponent my = new MyComponent();
            my.setBounds(0,0,screenWidth,screenHeight);
            this.add(my);
            init();
        });
    }
    public void init(){
        this.setLayout(null);
        this.setTitle("B1903_教务系统");
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setBackground(Color.white);
        this.setVisible(true);
        addComponent();
    }
    public void addComponent(){

        label_image1.setBounds(0,0,1920,219);
        label_image2.setBounds(0,219,1920,55);

        this.add(label_image1);
        this.add(label_image2);

        /*
        设置按钮透明
         */
//        btn1.setContentAreaFilled(false);
        /*
        取消边框
         */
//        btn1.setBorderPainted(false);

        btn1.setBounds(0,219+55+7,308,45);
        btn2.setBounds(0,219+10+(45+7)*2,308,45);
        btn3.setBounds(0,219+10+(45+7)*3,308,45);
        btn4.setBounds(0,219+10+(45+7)*4,308,45);
        btn5.setBounds(0,219+10+(45+7)*5,308,45);
        btn6.setBounds(0,219+10+(45+7)*6,308,45);
        btn7.setBounds(0,screenHeight-130,308,45);
        btn1.setFont(new Font("华文中宋",1,23));
        btn2.setFont(new Font("华文中宋",1,23));
        btn3.setFont(new Font("华文中宋",1,23));
        btn4.setFont(new Font("华文中宋",1,23));
        btn5.setFont(new Font("华文中宋",1,23));
        btn6.setFont(new Font("华文中宋",1,23));
        btn7.setFont(new Font("华文中宋",1,23));

        btn1.setBackground(new Color(149,181,252));
        btn2.setBackground(new Color(149,181,252));
        btn3.setBackground(new Color(149,181,252));
        btn4.setBackground(new Color(149,181,252));
        btn5.setBackground(new Color(149,181,252));
        btn6.setBackground(new Color(149,181,252));
        btn7.setBackground(new Color(149,181,252));

        this.add(btn1);
        this.add(btn2);
        this.add(btn3);
        this.add(btn4);
        this.add(btn5);
        this.add(btn6);
        this.add(btn7);

        btnHomePage.setBorderPainted(false);
        btnReturn.setBorderPainted(false);
        btnHomePage.setBackground(new Color(149,181,252));
        btnReturn.setBackground(new Color(149,181,252));
        btnHomePage.setFont(new Font("华文中宋",Font.PLAIN,24));
        btnReturn.setFont(new Font("华文中宋",Font.PLAIN,24));
        btnHomePage.setBounds(1720,227,90,40);
        btnReturn.setBounds(1810,227,100,40);
        this.add(btnHomePage);
        this.add(btnReturn);

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame1.this.setVisible(false);
                new StuFrame_Btn1();
            }
        });
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame1.this.setVisible(false);
                new StuFrame_Btn2();
            }
        });
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame1.this.setVisible(false);
                new StuFrame_Btn3();
            }
        });
        btn4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame1.this.setVisible(false);
                new StuFrame_Btn4();
            }
        });
        btn5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame1.this.setVisible(false);
                new StuFrame_Btn5();
            }
        });
        btn6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame1.this.setVisible(false);
                new StuFrame_Btn6();
            }
        });

        btn7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               System.exit(0);
            }
        });
    }
}

class MyComponent extends JComponent{
    public void paintComponent(Graphics g){
        Graphics2D g1=(Graphics2D)g;
//        Rectangle2D.Double  rect = new Rectangle2D.Double(0,219,1920,55);
//        g1.setPaint(new Color(149,181,252));
//        g1.fill(rect);
        g1.draw( new Line2D.Double(308,219+55,308,1080));
        g1.setFont(new Font("华文新魏",Font.PLAIN,32));
        g1.setColor(Color.WHITE);
        g1.drawString("叶鹏",1610,39);
        g1.setFont(new Font("华文中宋",Font.PLAIN,34));
        g1.setColor(Color.BLACK);
        g1.drawString("当前页面———首页",308,255);
    }
}


