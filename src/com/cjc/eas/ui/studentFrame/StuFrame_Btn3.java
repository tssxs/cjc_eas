package com.cjc.eas.ui.studentFrame;

import com.cjc.eas.service.studentService.StudentService;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;

/**
 * @ClassName StuFrame_Btn3_1
 * @Description TODO
 * @Author fades
 * @Date 2021/6/18 17:40
 * @Version 1.0
 **/

public class StuFrame_Btn3 extends  JFrame{
    public static void main(String[] args) {
        EventQueue.invokeLater(()->{
            new StuFrame_Btn3();
        });

    }
    String editValue = "";
    Icon image1 = new ImageIcon("E:\\eas图片\\学生栏目.jpg");
    Icon image2 = new ImageIcon("E:\\eas图片\\当前页面背景色.jpg");
    JLabel label_image1=new JLabel(image1);
    JLabel label_image2=new JLabel(image2);
    JButton btn1 = new JButton("公用信息");
    JButton btn2 = new JButton("信息查询");
    JButton btn3 = new JButton("网上选课");
    JButton btn4 = new JButton("活动报名");
    JButton btn5 = new JButton("网上教评");
    JButton btn6 = new JButton("账号管理");
    JButton btn7 = new JButton("安全退出");

    JButton btn3_1 = new JButton(" □ 预选");
    JButton btn3_2 = new JButton(" □ 补选");
    JButton btn3_3 = new JButton(" □ 退选");
    JButton btn3_4 = new JButton(" □ 查看结果");

    JButton btnHomePage = new JButton("首页");
    JButton btnReturn = new JButton("<返回");

    JButton btnSubmit = new JButton("提交");

    JScrollPane pane = null;
    JTable table = null;

    Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
    int screenWidth = (int) screensize.getWidth();//获得屏幕得宽
    int screenHeight = (int) screensize.getHeight();//获得屏幕得高

    public StuFrame_Btn3(){
        EventQueue.invokeLater(()->{
            MyComponent_Btn3_1 my = new MyComponent_Btn3_1();
            my.setBounds(0,0,screenWidth,screenHeight);
            this.add(my);
            init();
        });

    }
    public void init(){
        this.setLayout(null);
        this.setTitle("B1903_教务系统");
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setBackground(Color.white);
        this.setVisible(true);
        addComponent();
    }
    public void addComponent(){

        label_image1.setBounds(0,0,1920,219);
        label_image2.setBounds(0,219,1920,55);

        this.add(label_image1);
        this.add(label_image2);

        /*
        设置按钮透明
         */
//        btn1.setContentAreaFilled(false);
        /*
        取消边框
         */
//        btn1.setBorderPainted(false);

        btn1.setBounds(0,219+55+7,308,45);
        btn2.setBounds(0,219+10+(45+7)*2,308,45);
        btn3.setBounds(0,219+10+(45+7)*3,308,45);
        btn3_1.setBounds(0,219+20+(45+7)*3+35+4,208,35);
        btn3_2.setBounds(0,219+20+(45+7)*3+(35+4)*2,208,35);
        btn3_3.setBounds(0,219+20+(45+7)*3+(35+4)*3,208,35);
        btn3_4.setBounds(0,219+20+(45+7)*3+(35+4)*4,240,35);
        btn4.setBounds(0,219+10+(45+7)*3+(35+4)*4+(45+7),308,45);
        btn5.setBounds(0,219+10+(45+7)*3+(35+4)*4+(45+7)*2,308,45);
        btn6.setBounds(0,219+10+(45+7)*3+(35+4)*4+(45+7)*3,308,45);
        btn7.setBounds(0,screenHeight-130,308,45);



        btn1.setFont(new Font("华文中宋",1,23));
        btn3_1.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn3_2.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn3_3.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn3_4.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn2.setFont(new Font("华文中宋",1,23));
        btn3.setFont(new Font("华文中宋",1,23));
        btn4.setFont(new Font("华文中宋",1,23));
        btn5.setFont(new Font("华文中宋",1,23));
        btn6.setFont(new Font("华文中宋",1,23));
        btn7.setFont(new Font("华文中宋",1,23));

        btn1.setBackground(new Color(149,181,252));
        btn2.setBackground(new Color(149,181,252));
        btn3.setBackground(new Color(149,181,252));
        btn4.setBackground(new Color(149,181,252));
        btn5.setBackground(new Color(149,181,252));
        btn6.setBackground(new Color(149,181,252));
        btn7.setBackground(new Color(149,181,252));

        btn3_1.setBorderPainted(false);
        btn3_1.setContentAreaFilled(false);
        btn3_2.setBorderPainted(false);
        btn3_2.setContentAreaFilled(false);
        btn3_3.setBorderPainted(false);
        btn3_3.setContentAreaFilled(false);
        btn3_4.setBorderPainted(false);
        btn3_4.setContentAreaFilled(false);

        this.add(btn1);
        this.add(btn2);
        this.add(btn3);
        this.add(btn4);
        this.add(btn5);
        this.add(btn6);
        this.add(btn7);

        this.add(btn3_1);
        this.add(btn3_2);
        this.add(btn3_3);
        this.add(btn3_4);

        btnHomePage.setBorderPainted(false);
        btnReturn.setBorderPainted(false);
        btnHomePage.setBackground(new Color(149,181,252));
        btnReturn.setBackground(new Color(149,181,252));
        btnHomePage.setFont(new Font("华文中宋",Font.PLAIN,24));
        btnReturn.setFont(new Font("华文中宋",Font.PLAIN,24));
        btnHomePage.setBounds(1720,227,90,40);
        btnReturn.setBounds(1810,227,100,40);
        this.add(btnHomePage);
        this.add(btnReturn);

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn3.this.setVisible(false);
                new StuFrame_Btn1();
            }
        });
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn3.this.setVisible(false);
                new StuFrame_Btn2();
            }
        });
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn3.this.setVisible(false);
                new StuFrame1();
            }
        });
        btn4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn3.this.setVisible(false);
                new StuFrame_Btn4();
            }
        });
        btn5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn3.this.setVisible(false);
                new StuFrame_Btn5();
            }
        });
        btn6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StuFrame_Btn3.this.setVisible(false);
                new StuFrame_Btn6();
            }
        });
        btn7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        btn3_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showTable(StudentService.showCourselist_Service());
            }
        });

    }
    public void showTable(Object[][] o){
        TableModel_SCourseTable model = new TableModel_SCourseTable(o);
        table = new JTable(model){
            public void editingStopped(ChangeEvent changeevent){
                int r=getEditingRow();
                int c=getEditingColumn();
                System.out.println(r+"--"+c);
                TableCellEditor tablecelleditor = getCellEditor();
                if(tablecelleditor != null)
                {
                    Object obj = tablecelleditor.getCellEditorValue();
                    setValueAt(obj, editingRow, editingColumn);
                    removeEditor();
                }
                editValue = table.getValueAt(r, c).toString();
                System.out.println(editValue);
            }
        };
        //设置表头高度40
        table.getTableHeader().setPreferredSize(new Dimension(1,40));
        //设置行高40
        table.setRowHeight(TableModel_SCourseTable.rowHeight);
        table.getColumnModel().getColumn(0).setMinWidth(103);
        table.getColumnModel().getColumn(1).setMinWidth(138);
        table.getColumnModel().getColumn(2).setMinWidth(275);
        table.getColumnModel().getColumn(3).setMinWidth(167);
        table.getColumnModel().getColumn(4).setMinWidth(174);
        table.getColumnModel().getColumn(5).setMinWidth(288);
        table.getColumnModel().getColumn(6).setMinWidth(192);
        table.getColumnModel().getColumn(7).setMinWidth(96);

        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();// 设置table内容居中
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        table.setDefaultRenderer(Object.class, tcr);

        table.getColumnModel().getColumn(0).setCellRenderer(tcr);

        //设置表格的背景颜色和字体
        table.setBackground(Color.WHITE);
        table.getTableHeader().setBackground(Color.WHITE);
        table.getTableHeader().setFont(new Font("华文仿宋",1,24));
        table.setFont(new Font("华文仿宋",1,24));

        JCheckBox check = new JCheckBox("");
        DefaultCellEditor cellEditor = new DefaultCellEditor(check);
        cellEditor.stopCellEditing();
        table.getColumnModel().getColumn(7).setCellEditor(cellEditor);

        pane = new JScrollPane(table);
        pane.setBounds(381,399,1438,480);
        this.add(pane);
        btnSubmit.setBounds(1690,890,130,40);
        btnSubmit.setFont(new Font("黑体",Font.PLAIN,28));
        btnSubmit.setBackground(new Color(149,181,252));
        this.add(btnSubmit);

        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

    }
}

class MyComponent_Btn3_1 extends JComponent {
    public void paintComponent(Graphics g){
        Graphics2D g1=(Graphics2D)g;
        g1.draw( new Line2D.Double(308,219+55,308,1080));
        g1.setFont(new Font("华文新魏",Font.PLAIN,32));
        g1.setColor(Color.WHITE);
        g1.drawString("叶鹏",1610,39);
        g1.setFont(new Font("华文中宋",Font.PLAIN,34));
        g1.setColor(Color.BLACK);
        g1.drawString("当前页面———网上选课",308,255);
        g1.setFont(new Font("黑体",1,36));
        g1.drawString("课程清单",917,360);
    }
}

class TableModel_SCourseTable extends AbstractTableModel {
    static int rowHeight = 40;

    private Object[][] cells;
    public TableModel_SCourseTable(Object[][] o){
        cells=o;
    }
    private static String[] columnNames = {"序号","课程号","课程名","类别","授课教师","开设院系","课时","Y/N"};

    static int weight=columnNames.length;
    @Override
    public int getRowCount() {
//        System.out.println(cells.length);
        return cells.length;
    }

    @Override
    public int getColumnCount() {
//        System.out.println(cells[0].length);
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return  cells[rowIndex][columnIndex];
    }

    public void setValueAt(Object obj,int rowIndex,int columnIndex){
        cells[rowIndex][columnIndex]=obj;
    }

    public String getColumnName(int c){
        return columnNames[c];
    }

    public Class<?> getColumnClass(int columnIndex){
        return cells[0][columnIndex].getClass();
    }

    public boolean isCellEditable(int rowIndex,int columnIndex){
        return columnIndex == 7;
    }
}
