package com.cjc.eas.ui.loginFrame;

/**
 * @ClassName LojinWin
 * @Description TODO 登录窗口
 * @Author fades
 * @Date 2021/6/1 20:57
 * @Version 1.0
 **/

import com.cjc.eas.dao.loginDao.AdminLogin;
import com.cjc.eas.dao.loginDao.StudentLogin;
import com.cjc.eas.dao.loginDao.TeacherLogin;
import com.cjc.eas.ui.studentFrame.StuFrame1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginWin extends JFrame{
    Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
    int screenWidth = (int) screensize.getWidth();//获得屏幕得宽
    int screenHeight = (int) screensize.getHeight();//获得屏幕得高
    Icon image1 = new ImageIcon("E:\\eas图片\\登陆图标.jpg");
    Icon image2 = new ImageIcon("E:\\eas图片\\密码图标.jpg");
    Icon image3 = new ImageIcon("E:\\eas图片\\验证码图标.jpg");
    JLabel label_image1=new JLabel(image1);
    JLabel label_image2=new JLabel(image2);
    JLabel label_image3=new JLabel(image3);
    JLabel label_txt1=new JLabel("用户登录/LOGIN");
    JLabel label_txt2=new JLabel("用户名:");
    JLabel label_txt3=new JLabel("密 码:");
    JLabel label_txt4=new JLabel("验证码:");
    JTextField TextField1=new JTextField(11);
    //JTextField TextFiedl2=new JTextField(18);
    JPasswordField TextFiedl2 = new JPasswordField();
    JTextField TextField3=new JTextField(4);
    JButton btn1 = new JButton("登录");
    JButton btn2 = new JButton("忘记密码?");

    public LoginWin(){
        EventQueue.invokeLater(()->{
            init();
            addComponent();
            this.TextFiedl2.setEchoChar('*');
        });
    }
    public void init(){
        setTitle("昌吉学院教务管理系统");
        this.setLayout(null);
        //让该窗口显示在中间
        this.setBounds((screenWidth-450)/2,(screenHeight-420)/2,450,420);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addComponent(){
        btn1.setBounds(55,295,330,40);
        btn1.setBackground(new Color(205,221,230));
        this.add(btn1);
        label_txt1.setBounds(55,10,350,56);
        label_txt1.setFont(new Font("华文隶书",1,42));
        label_txt1.setForeground(new Color(19,0x7a,0xd4));
        label_image1.setBounds(65,90,45,29);
        label_txt2.setBounds(120,90,87,29);
        label_txt2.setFont(new Font("华文中宋",Font.PLAIN,22));
        TextField1.setBounds(210,90,180,29);
        label_image2.setBounds(65,140,45,29);
        label_image3.setBounds(65,190,45,29);
        label_txt3.setBounds(120,140,87,29);
        label_txt3.setFont(new Font("华文中宋",Font.PLAIN,22));
        TextFiedl2.setBounds(210,140,180,29);
        label_txt4.setBounds(120,190,87,29);
        label_txt4.setFont(new Font("华文中宋",Font.PLAIN,22));
        TextField3.setBounds(210,190,90,29);

        this.add(label_txt1);
        this.add(label_image1);
        this.add(label_txt2);
        this.add(TextField1);
        this.add(label_image2);
        this.add(label_image3);
        this.add(label_txt3);
        this.add(TextFiedl2);
        this.add(label_txt4);
        this.add(TextField3);

        addJRadionButton();
        panel.setBounds(65,230,300,30);
        panel.setFont(new Font("华文中宋",Font.PLAIN,22));
        this.add(panel);

        btn2.setBounds(300,260,105,16);
        btn2.setForeground(new Color(19,0x7a,0xd4));
        btn2.setFont(new Font("华文中宋",1,14));
        btn2.setBorderPainted(false);
        btn2.setContentAreaFilled(false);
        this.add(btn2);

    }
    /*
     *单选框
     */
    JPanel panel = new JPanel();
    public void addJRadionButton(){
        ButtonGroup group = new ButtonGroup();
        JRadioButton radioButton1 = new JRadioButton("学生");
        JRadioButton radioButton2 = new JRadioButton("教师");
        JRadioButton radioButton3 = new JRadioButton("管理员");
        group.add(radioButton1);
        group.add(radioButton2);
        group.add(radioButton3);
        panel.add(radioButton1);
        panel.add(radioButton2);
        panel.add(radioButton3);

        radioButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btn1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        /*
                        验证码 未判断
                         */
                        if(StudentLogin.studentLogin(TextField1.getText(),TextFiedl2.getText())){
                            new StuFrame1();
                            LoginWin.this.setVisible(false);
                        }
                    }
                });
            }
        });
        radioButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btn1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        /*
                        验证码 未判断
                         */
                        TeacherLogin.teacherLogin(TextField1.getText(),TextFiedl2.getText());
                    }
                });
            }
        });
        radioButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btn1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        /*
                        验证码 未判断
                         */
                        AdminLogin.adminLogin(TextField1.getText(),TextFiedl2.getText());
                    }
                });
            }
        });
    }

    public static void main(String[] args) {
        new LoginWin();
    }
}

