package com.cjc.eas.ui.teacherFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;

/**
 * @ClassName TchFrame_Btn5
 * @Description TODO
 * @Author fades
 * @Date 2021/6/18 20:55
 * @Version 1.0
 **/

public class TchFrame_Btn5 extends JFrame{
    public static void main(String[] args) {
        new TchFrame_Btn5();
    }

    Icon image1 = new ImageIcon("E:\\eas图片\\学生栏目.jpg");
    Icon image2 = new ImageIcon("E:\\eas图片\\当前页面背景色.jpg");
    JLabel label_image1=new JLabel(image1);
    JLabel label_image2=new JLabel(image2);
    JButton btn1 = new JButton("公用信息");
    JButton btn2 = new JButton("信息查询");
    JButton btn3 = new JButton("教学管理");
    JButton btn4 = new JButton("课程管理");
    JButton btn5 = new JButton("项目信息");
    JButton btn6 = new JButton("账号管理");
    JButton btn7 = new JButton("安全退出");

    JButton btn5_1 = new JButton(" □ 申报项目");
    JButton btn5_2 = new JButton(" □ 查看项目");

    JButton btnHomePage = new JButton("首页");
    JButton btnReturn = new JButton("<返回");

    Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
    int screenWidth = (int) screensize.getWidth();//获得屏幕得宽
    int screenHeight = (int) screensize.getHeight();//获得屏幕得高

    public TchFrame_Btn5(){
        EventQueue.invokeLater(()->{
            MyComponent_Btn5 my = new MyComponent_Btn5();
            my.setBounds(0,0,screenWidth,screenHeight);
            this.add(my);
            init();
        });

    }
    public void init(){
        this.setLayout(null);
        this.setTitle("B1903_教务系统");
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setBackground(Color.white);
        this.setVisible(true);
        addComponent();
    }
    public void addComponent(){

        label_image1.setBounds(0,0,1920,219);
        label_image2.setBounds(0,219,1920,55);

        this.add(label_image1);
        this.add(label_image2);

        /*
        设置按钮透明
         */
//        btn1.setContentAreaFilled(false);
        /*
        取消边框
         */
//        btn1.setBorderPainted(false);

        btn1.setBounds(0,219+55+7,308,45);
        btn2.setBounds(0,219+10+(45+7)*2,308,45);
        btn3.setBounds(0,219+10+(45+7)*3,308,45);
        btn4.setBounds(0,219+10+(45+7)*4,308,45);
        btn5.setBounds(0,219+10+(45+7)*5,308,45);
        btn5_1.setBounds(0,219+20+(45+7)*5+35+4,208,35);
        btn5_2.setBounds(0,219+20+(45+7)*5+(35+4)*2,208,35);
        btn6.setBounds(0,219+10+(45+7)*5+(35+4)*2+45+7,308,45);
        btn7.setBounds(0,screenHeight-130,308,45);



        btn1.setFont(new Font("华文中宋",1,23));
        btn5_1.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn5_2.setFont(new Font("华文中宋",Font.PLAIN,16));
        btn2.setFont(new Font("华文中宋",1,23));
        btn3.setFont(new Font("华文中宋",1,23));
        btn4.setFont(new Font("华文中宋",1,23));
        btn5.setFont(new Font("华文中宋",1,23));
        btn6.setFont(new Font("华文中宋",1,23));
        btn7.setFont(new Font("华文中宋",1,23));

        btn1.setBackground(new Color(149,181,252));
        btn2.setBackground(new Color(149,181,252));
        btn3.setBackground(new Color(149,181,252));
        btn4.setBackground(new Color(149,181,252));
        btn5.setBackground(new Color(149,181,252));
        btn6.setBackground(new Color(149,181,252));
        btn7.setBackground(new Color(149,181,252));

        btn5_1.setBorderPainted(false);
        btn5_1.setContentAreaFilled(false);
        btn5_2.setBorderPainted(false);
        btn5_2.setContentAreaFilled(false);

        this.add(btn1);
        this.add(btn2);
        this.add(btn3);
        this.add(btn4);
        this.add(btn5);
        this.add(btn6);
        this.add(btn7);

        this.add(btn5_1);
        this.add(btn5_2);

        btnHomePage.setBorderPainted(false);
        btnReturn.setBorderPainted(false);
        btnHomePage.setBackground(new Color(149,181,252));
        btnReturn.setBackground(new Color(149,181,252));
        btnHomePage.setFont(new Font("华文中宋",Font.PLAIN,24));
        btnReturn.setFont(new Font("华文中宋",Font.PLAIN,24));
        btnHomePage.setBounds(1720,227,90,40);
        btnReturn.setBounds(1810,227,100,40);
        this.add(btnHomePage);
        this.add(btnReturn);

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TchFrame_Btn5.this.setVisible(false);
                new TchFrame_Btn1();
            }
        });
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TchFrame_Btn5.this.setVisible(false);
                new TchFrame_Btn2();
            }
        });
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TchFrame_Btn5.this.setVisible(false);
                new TchFrame_Btn3();
            }
        });
        btn4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TchFrame_Btn5.this.setVisible(false);
                new TchFrame_Btn4();
            }
        });
        btn5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TchFrame_Btn5.this.setVisible(false);
                new TchFrame1();
            }
        });
        btn6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TchFrame_Btn5.this.setVisible(false);
                new TchFrame_Btn6();
            }
        });
        btn7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

    }
}

class MyComponent_Btn5 extends JComponent{
    public void paintComponent(Graphics g){
        Graphics2D g1=(Graphics2D)g;
//        Rectangle2D.Double  rect = new Rectangle2D.Double(0,219,1920,55);
//        g1.setPaint(new Color(149,181,252));
//        g1.fill(rect);
        g1.draw( new Line2D.Double(308,219+55,308,1080));
        g1.setFont(new Font("华文新魏",Font.PLAIN,32));
        g1.setColor(Color.WHITE);
        g1.drawString("叶鹏",1610,39);
        g1.setFont(new Font("华文中宋",Font.PLAIN,34));
        g1.setColor(Color.BLACK);
        g1.drawString("当前页面———项目信息",308,255);
    }
}
