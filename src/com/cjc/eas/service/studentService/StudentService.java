package com.cjc.eas.service.studentService;

import com.cjc.eas.dao.studentDao.StudentDao;
import com.cjc.eas.domain.Course;
import com.cjc.eas.domain.Score;
import org.junit.Test;

import java.util.ArrayList;

/**
 * @ClassName studentService
 * @Description TODO
 * @Author fades
 * @Date 2021/6/18 16:37
 * @Version 1.0
 **/

public class StudentService {

    /**
      *@Description 将dao包中的方法的返回类型进行转换
      *@params
      *@return
      *@author fades
      *@since
      */
    public static Object[][] queryScore_Service(String scoAnnual,String scoPeriod,String scoSno){
        Object[][] result ;
        ArrayList<Score> arrayList = StudentDao.queryScore(scoAnnual,scoPeriod,scoSno);
        result = new Object[arrayList.size()][10];
        for(int i=0;i<arrayList.size();i++){
            result[i][0]=i+1;
            result[i][1]=arrayList.get(i).getScoCno();
            result[i][2]=arrayList.get(i).getScoName();
            result[i][3]=arrayList.get(i).getScoGetCredits();
            result[i][4]=arrayList.get(i).getScoType();
            result[i][5]=arrayList.get(i).getScoTeacher();
            result[i][6]=arrayList.get(i).getScoDepartment();
            result[i][7]=arrayList.get(i).getScoScore();
            result[i][8]=arrayList.get(i).getScoGPA();
        }
        return result;
    }

    @Test
    public void test1(){
        Object[][] re =queryScore_Service("2020-2021","第二学期","1945867001");
        for(int i=0;i<re.length;i++){
            for(int j=0;j<re[0].length;j++){
                System.out.print(re[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    public static Object[][] showCourselist_Service(){
        Object[][] result;
        ArrayList<Course> courseArrayList = StudentDao.showCourselist();
        result = new Object[courseArrayList.size()][8];
        for(int i=0;i<courseArrayList.size();i++){
            result[i][0]=i+1;
            result[i][1]=courseArrayList.get(i).getCouCno();
            result[i][2]=courseArrayList.get(i).getCouName();
            result[i][3]=courseArrayList.get(i).getCouType();
            result[i][4]=courseArrayList.get(i).getCouTeacher();
            result[i][5]=courseArrayList.get(i).getCouDepartment();
            result[i][6]=courseArrayList.get(i).getCouTime();
            result[i][7]=false;
        }
        return  result;
    }
    @Test
    public void test2(){
        Object[][] re =showCourselist_Service();
        for(int i=0;i<re.length;i++){
            for(int j=0;j<re[0].length;j++){
                System.out.print(re[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
